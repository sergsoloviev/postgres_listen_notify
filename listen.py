#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pprint import pprint
import json
import select
import psycopg2
import psycopg2.extensions


def main():
    conn = psycopg2.connect(
        host='192.168.70.70',
        database='test1',
        user='postgres',
        password='sql',
    )
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

    curs = conn.cursor()
    curs.execute("LISTEN test;")

    print("Waiting for notifications on channel 'test'")
    while 1:
        if select.select([conn],[],[],5) == ([],[],[]):
            # print("Timeout")
            pass
        else:
            conn.poll()
            while conn.notifies:
                notify = conn.notifies.pop(0)
                print("Got NOTIFY:", notify.pid, notify.channel, notify.payload)
                try:
                    d = json.loads(notify.payload)
                    pprint(d)
                except:
                    print("no json in payload")





if __name__ == '__main__':
    main()
