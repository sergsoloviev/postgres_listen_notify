#!/usr/bin/env python
# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extensions
import json


def main():
    conn = psycopg2.connect(
        host='192.168.70.70',
        database='test1',
        user='postgres',
        password='sql',
    )
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

    curs = conn.cursor()

    message = {
        'testfield': 'testval',
        'поле': 'значение',
    }

    curs.execute("NOTIFY %s, '%s';" % ('test', json.dumps(message)))
    conn.close()

if __name__ == '__main__':
    main()
